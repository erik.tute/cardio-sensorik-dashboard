# Cardio Sensorik Dashboard
Dashboard to assist local study nurses in *HiGHmed Cardiology Use Case* in monitoring incoming research data from study participants equipped with a mobile device and a smartwatch.

Intended use: To monitor that expected data for participants arrives in decent quality. 

Misuse: This dashboard is not intended for telemonitoring or anything in this direction.

## Installation

### Prerequisites
You need [RStudio](https://www.rstudio.com) with packages [Shiny](https://shiny.posit.co/), [DT](https://rstudio.github.io/DT/) and [ggplot2](https://ggplot2.tidyverse.org/) installed.

### Installation
Now you can:
1. Download the project. (Obviously, you can also use git for this part.)
2. Download the [R-files](https://gitlab.gwdg.de/erik.tute/opencqa-notebook/-/tree/main/R?ref_type=heads) from my [openCQA Notebook](https://gitlab.gwdg.de/erik.tute/opencqa-notebook/) project.
3. Add the R-files to the directory named ``R`` in your project.
4. Furthermore, you probably want to include a questionaire_strings.R file, which I can't provide with the rest of the dashboard as open source. If you have trouble with that part, you can simply comment the line ``source("R/questionaire_strings.R")`` and use the dashboard without being able to inspect the PRO questionnaire.
5. You need an .Renviron file specifying the following values for your local environment:

```
queryset = "better"
query_url = "https://<<your_server>>/rest/openehr/v1/query/aql"
query_user = "<<your_user>>"
query_key = "<<your_user>>"
shiny_version = "current"
```

Now you can run the dashboard like any other R-Shiny app, e.g. by opening the app.R in RStudio and clicking on ``Run App``.

## License
MIT License

## Project status
Startet in January 2024.

## Example
Screenshot of the dashboard showing overview for multiple participants:
![Example screenshot overview](img/example_overview.png)

Screenshot showing visualization for blood pressure, pulse and weight values of one participant:
![Example screenshot dp_etc](img/example_bp_pulse_weight.png)

Screenshot showing visualization for step counts of one participant:
![Example screenshot steps](img/example_steps.png)

